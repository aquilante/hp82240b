#-----------------------------------------------------------------------------
# Name:        HP82240B.py
# Purpose:     
#
# Author:      Alfredo Branco
#
# Created:     2011/08/14
# RCS-ID:      $Id: HP82240B.py $
# Copyright:   (c) 2006
# Licence:     GPL
#-----------------------------------------------------------------------------
#!/usr/bin/env python
#Boa:App:BoaApp

import wx
import time
import traceback
import sys

import gettext
gettext.bindtextdomain('HP82240Bemu', './lang')
gettext.textdomain('HP82240Bemu')
_ = gettext.lgettext

import Printer

modules ={u'Printer': [1, 'Main frame of Application', u'Printer.py'],
 u'PyRTFParser': [0, '', u'PyRTFParser.py'],
 u'messages': [0, '', u'messages.pot'],
 u'wxSerialConfigDialog': [0, '', u'wxSerialConfigDialog.py']}


def exceptHook(type, value, trace):
    if wx and sys and traceback:
        exc=traceback.format_exception(type, value, trace)
        for e in exc:
            wx.LogError(e)
        wx.LogError('Unhandled ERROR: %s - %s' % (str(type), str(value)))
        sys.__excepthook__(type, value,trace)

class BoaApp(wx.App):
    def OnInit(self):
        
        self.main = Printer.create(None)
        self.main.Show()
        self.SetTopWindow(self.main)

        self.keepGoing = True
        return True

    def NoMainLoop(self):

        # Create an event loop and make it active.  If you are
        # only going to temporarily have a nested event loop then
        # you should get a reference to the old one and set it as
        # the active event loop when you are done with this one...
        evtloop = wx.EventLoop()
        old = wx.EventLoop.GetActive()
        wx.EventLoop.SetActive(evtloop)

        # This outer loop determines when to exit the application,
        # for this example we let the main frame reset this flag
        # when it closes.
        while self.keepGoing:
            # At this point in the outer loop you could do
            # whatever you implemented your own MainLoop for.  It
            # should be quick and non-blocking, otherwise your GUI
            # will freeze.  

            # call_your_code_here()


            # This inner loop will process any GUI events
            # until there are no more waiting.
            while evtloop.Pending():
                evtloop.Dispatch()

            # Send idle events to idle handlers.  You may want to
            # throttle this back a bit somehow so there is not too
            # much CPU time spent in the idle handlers.  For this
            # example, I'll just snooze a little...
            time.sleep(0.10)
            self.ProcessIdle()

        wx.EventLoop.SetActive(old)


def main():
    
    sys.excepthook=exceptHook
    
    application = BoaApp(0)
    application.MainLoop()

if __name__ == '__main__':
    main()
