#-----------------------------------------------------------------------------
# Name:        Printer.py
# Purpose:     
#
# Author:      Alfredo Branco
#
# Created:     2011/08/14
# RCS-ID:      $Id: HP82240B.py $
# Copyright:   (c) 2006
# Licence:     GPL
#-----------------------------------------------------------------------------
#Boa:Frame:PrinterFrame

################## CREDITS ######################
## This contains a wxPython PyRichTextRTFHandler written by David Woods as part of Transana.
## It's designed, though, to be used independently with any wxRichTextCtrl-based program.
## This code is covered by the GNU GPL. Code modifications are welcome, and are encouraged.
##
##	David Woods (dwoods@wcer.wisc.edu)
#################################################

_SW_RELEASE="0.2a"

import gettext
gettext.bindtextdomain('HP82240Bemu', './lang')
gettext.textdomain('HP82240Bemu')
_ = gettext.lgettext

import os
import ConfigParser


import wx
import wx.richtext
from wx.lib.wordwrap import wordwrap

# import the PyRTFParser
import PyRTFParser

##To save the contents of a wxRichTextCtrl control called self.Paper into a Rich Text Format file called path:
##
### Use the custom RTF Handler
##handler = PyRTFParser.PyRichTextRTFHandler()
### Save the file with the custom RTF Handler.
### The custom RTF Handler can take either a wxRichTextCtrl or a wxRichTextBuffer argument.
##handler.SaveFile(self.Paper.GetBuffer(), path)
##
##To load the contents of a Rich Text Format file called path into a wxRichTextCtrl control called self.Paper:
##
### Use the custom RTF Handler
##handler = PyRTFParser.PyRichTextRTFHandler()
### Load the RTF file via the RTF Handler.
### Note that for RTF, the wxRichTextCtrl CONTROL is passed.
##handler.LoadFile(self.Paper, path)
##
##Alternately, if you have the RTF data in the form of a string called str (from a database, for example), you can use the following:
##
### Use the custom RTF Handler
##handler = PyRTFParser.PyRichTextRTFHandler()
### Load the RTF file via the RTF Handler.
### Note that for RTF, the wxRichTextCtrl CONTROL is passed.
##handler.LoadString(self.Paper, str)



import usb.core
import usb.util
import serial
from wxSerialConfigDialog import SerialConfigDialog

import traceback
import time

import sys
import ctypes

import math


import subprocess
import threading
PIPE=subprocess.PIPE

import types

sres= unicode()


from PIL import Image

def WxBitmapToPilImage( myBitmap ) :
    return WxImageToPilImage( WxBitmapToWxImage( myBitmap ) )

def WxBitmapToWxImage( myBitmap ) :
    return wx.ImageFromBitmap( myBitmap )

#-----

def PilImageToWxBitmap( myPilImage ) :
    return WxImageToWxBitmap( PilImageToWxImage( myPilImage ) )

def PilImageToWxImage( myPilImage ):
    myWxImage = wx.EmptyImage( myPilImage.size[0], myPilImage.size[1] )
    myWxImage.SetData( myPilImage.convert( 'RGB' ).tostring() )
    return myWxImage

class subprocessOutputRead(threading.Thread):
    def __init__ (self, dev):
        threading.Thread.__init__(self)
        self.status=-1
        self.dev=dev
        self.startTime=time.time()
        if isinstance(dev, serial.Serial):
            self.run=self.runSerial
        else:
            self.run=self.runUSB
       
    def runSerial(self):
        res=unicode()
        global ctrl
        global sres
        ctrl=(-116,"")
        sres=unicode(self.dev.readline(),'iso-8859-1')
        #try:
        while True:
            res=unicode(self.dev.readline(),'iso-8859-1')
            sres+=res
            if len(res) > 0:
                self.startTime=time.time()
            if time.time()-self.startTime > 3:
                return
        #except usb.legacy.USBError:
        #    exc=sys.exc_info()[1]
        #    ctrl=(exc.errno, exc.strerror)

    def runUSB(self):
        res=unicode()
        global ctrl
        global sres
        try:
            sres=unicode(self.dev.read(0x81,128,0,10000).tostring(),'iso-8859-1')
            while True:
                res=unicode(self.dev.read(0x81,128,0,3000).tostring(),'iso-8859-1')
                sres+=res
        except usb.legacy.USBError:
            exc=sys.exc_info()[1]
            ctrl=(exc.errno, exc.strerror)



def create(parent):
    return PrinterFrame(parent)

[wxID_PRINTERFRAME, wxID_PRINTERFRAMEBACKPANEL, wxID_PRINTERFRAMECRBTN, 
 wxID_PRINTERFRAMEPANEL1, wxID_PRINTERFRAMEPANEL10, wxID_PRINTERFRAMEPANEL2, 
 wxID_PRINTERFRAMEPANEL3, wxID_PRINTERFRAMEPANEL4, wxID_PRINTERFRAMEPANEL5, 
 wxID_PRINTERFRAMEPANEL6, wxID_PRINTERFRAMEPANEL7, wxID_PRINTERFRAMEPANEL8, 
 wxID_PRINTERFRAMEPANEL9, wxID_PRINTERFRAMEPAPER, wxID_PRINTERFRAMEPOWERBTN, 
 wxID_PRINTERFRAMESTATUSBAR, 
] = [wx.NewId() for _init_ctrls in range(16)]

[wxID_PRINTERFRAMECONNECTMNUITEMS0, wxID_PRINTERFRAMECONNECTMNUITEMS1, 
] = [wx.NewId() for _init_coll_ConnectMnu_Items in range(2)]

[wxID_PRINTERFRAMEEDITMNUMENU_EDIT_CLEAR, 
 wxID_PRINTERFRAMEEDITMNUMENU_EDIT_COPY, 
 wxID_PRINTERFRAMEEDITMNUMENU_EDIT_CUT, 
 wxID_PRINTERFRAMEEDITMNUMENU_EDIT_PASTE, 
] = [wx.NewId() for _init_coll_EditMnu_Items in range(4)]

[wxID_PRINTERFRAMEHELPMNUINFO] = [wx.NewId() for _init_coll_HelpMnu_Items in range(1)]

[wxID_PRINTERFRAMEFILEMNUFONTITEM] = [wx.NewId() for _init_coll_FileMnu_Items in range(1)]

[wxID_PRINTERFRAMEPOLLTIMER] = [wx.NewId() for _init_utils in range(1)]

class PrinterFrame(wx.Frame):
    def _init_coll_boxSizer6_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.Paper, 1, border=0, flag=wx.EXPAND | wx.GROW)

    def _init_coll_boxSizer4_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.panel9, 1, border=0, flag=wx.EXPAND | wx.GROW)
        parent.AddWindow(self.panel7, 0, border=0, flag=0)

    def _init_coll_boxSizer5_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.panel10, 1, border=0, flag=wx.GROW | wx.EXPAND)
        parent.AddWindow(self.panel8, 0, border=0, flag=0)

    def _init_coll_boxSizer3_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.panel3, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.panel5, 1, border=0, flag=wx.GROW | wx.EXPAND)
        parent.AddWindow(self.panel6, 0, border=0, flag=wx.EXPAND)

    def _init_coll_boxSizer1_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.BackPanel, 1, border=0, flag=wx.GROW | wx.EXPAND)

    def _init_coll_boxSizer2_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.panel2, 1, border=0, flag=wx.GROW | wx.EXPAND)
        parent.AddWindow(self.panel1, 0, border=0, flag=wx.EXPAND)

    def _init_coll_FileMnu_Items(self, parent):
        # generated method, don't edit

        parent.AppendSeparator()
        parent.Append(help='', id=wxID_PRINTERFRAMEFILEMNUFONTITEM,
              kind=wx.ITEM_NORMAL, text=_('Font Setup'))
        self.Bind(wx.EVT_MENU, self.OnFileMnuFontitemMenu,
              id=wxID_PRINTERFRAMEFILEMNUFONTITEM)

    def _init_coll_ConnectMnu_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_PRINTERFRAMECONNECTMNUITEMS0,
              kind=wx.ITEM_NORMAL, text=_('USB'))
        parent.Append(help='', id=wxID_PRINTERFRAMECONNECTMNUITEMS1,
              kind=wx.ITEM_NORMAL, text=_('Serial port...'))
        self.Bind(wx.EVT_MENU, self.OnConnectMnuItems0Menu,
              id=wxID_PRINTERFRAMECONNECTMNUITEMS0)
        self.Bind(wx.EVT_MENU, self.OnConnectMnuItems1Menu,
              id=wxID_PRINTERFRAMECONNECTMNUITEMS1)

    def _init_coll_menuBar1_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.FileMnu, title=_('File'))
        parent.Append(menu=self.EditMnu, title=_('Edit'))
        parent.Append(menu=self.ConnectMnu, title=_('Connect to...'))
        parent.Append(menu=self.HelpMnu, title=_('Help'))

    def _init_coll_HelpMnu_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_PRINTERFRAMEHELPMNUINFO,
              kind=wx.ITEM_NORMAL, text=_('Info'))
        self.Bind(wx.EVT_MENU, self.OnHelpMnuInfoMenu,
              id=wxID_PRINTERFRAMEHELPMNUINFO)

    def _init_coll_EditMnu_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_PRINTERFRAMEEDITMNUMENU_EDIT_COPY,
              kind=wx.ITEM_NORMAL, text='Copy')
        parent.Append(help='', id=wxID_PRINTERFRAMEEDITMNUMENU_EDIT_CUT,
              kind=wx.ITEM_NORMAL, text='Cut')
        parent.Append(help='', id=wxID_PRINTERFRAMEEDITMNUMENU_EDIT_PASTE,
              kind=wx.ITEM_NORMAL, text='Paste')
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_PRINTERFRAMEEDITMNUMENU_EDIT_CLEAR,
              kind=wx.ITEM_NORMAL, text='Clear')
        self.Bind(wx.EVT_MENU, self.OnEditMnuMenu_edit_clearMenu,
              id=wxID_PRINTERFRAMEEDITMNUMENU_EDIT_CLEAR)

    def _init_utils(self):
        # generated method, don't edit
        self.menuBar1 = wx.MenuBar()

        self.PollTimer = wx.Timer(id=wxID_PRINTERFRAMEPOLLTIMER, owner=self)
        self.Bind(wx.EVT_TIMER, self.OnPollTimerTimer,
              id=wxID_PRINTERFRAMEPOLLTIMER)

        self.ConnectMnu = wx.Menu(title='')

        self.HelpMnu = wx.Menu(title='')

        self.EditMnu = wx.Menu(title='')

        self.FileMnu = wx.Menu(title='')

        self._init_coll_menuBar1_Menus(self.menuBar1)
        self._init_coll_ConnectMnu_Items(self.ConnectMnu)
        self._init_coll_HelpMnu_Items(self.HelpMnu)
        self._init_coll_EditMnu_Items(self.EditMnu)
        self._init_coll_FileMnu_Items(self.FileMnu)

    def _init_sizers(self):
        # generated method, don't edit
        self.boxSizer1 = wx.BoxSizer(orient=wx.VERTICAL)

        self.boxSizer2 = wx.BoxSizer(orient=wx.VERTICAL)

        self.boxSizer3 = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.boxSizer4 = wx.BoxSizer(orient=wx.VERTICAL)

        self.boxSizer5 = wx.BoxSizer(orient=wx.VERTICAL)

        self.boxSizer6 = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_boxSizer1_Items(self.boxSizer1)
        self._init_coll_boxSizer2_Items(self.boxSizer2)
        self._init_coll_boxSizer3_Items(self.boxSizer3)
        self._init_coll_boxSizer4_Items(self.boxSizer4)
        self._init_coll_boxSizer5_Items(self.boxSizer5)
        self._init_coll_boxSizer6_Items(self.boxSizer6)

        self.SetSizer(self.boxSizer1)
        self.BackPanel.SetSizer(self.boxSizer2)
        self.panel2.SetSizer(self.boxSizer3)
        self.panel3.SetSizer(self.boxSizer4)
        self.panel5.SetSizer(self.boxSizer6)
        self.panel6.SetSizer(self.boxSizer5)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_PRINTERFRAME, name='PrinterFrame',
              parent=prnt, pos=wx.Point(21, 26), size=wx.Size(279, 587),
              style=wx.DEFAULT_FRAME_STYLE, title='HP82240B Emulator')
        self._init_utils()
        self.SetClientSize(wx.Size(263, 549))
        self.SetMenuBar(self.menuBar1)

        self.BackPanel = wx.Panel(id=wxID_PRINTERFRAMEBACKPANEL,
              name='BackPanel', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(263, 549), style=wx.TAB_TRAVERSAL)

        self.panel1 = wx.Panel(id=wxID_PRINTERFRAMEPANEL1, name='panel1',
              parent=self.BackPanel, pos=wx.Point(0, 399), size=wx.Size(263,
              150), style=wx.TAB_TRAVERSAL)
        self.panel1.SetBackgroundColour(wx.Colour(55, 32, 0))
        self.panel1.SetMinSize(wx.Size(250, 150))

        self.panel4 = wx.Panel(id=wxID_PRINTERFRAMEPANEL4, name='panel4',
              parent=self.BackPanel, pos=wx.Point(400, 0), size=wx.Size(0, 546),
              style=wx.TAB_TRAVERSAL)
        self.panel4.SetBackgroundColour(wx.Colour(255, 255, 255))

        self.panel2 = wx.Panel(id=wxID_PRINTERFRAMEPANEL2, name='panel2',
              parent=self.BackPanel, pos=wx.Point(0, 0), size=wx.Size(263, 399),
              style=wx.TAB_TRAVERSAL)
        self.panel2.SetBackgroundColour(wx.Colour(240, 240, 240))

        self.panel3 = wx.Panel(id=wxID_PRINTERFRAMEPANEL3, name='panel3',
              parent=self.panel2, pos=wx.Point(0, 0), size=wx.Size(32, 399),
              style=wx.TRANSPARENT_WINDOW | wx.TAB_TRAVERSAL)
        self.panel3.SetBackgroundColour(wx.Colour(240, 240, 240))
        self.panel3.SetBackgroundStyle(wx.BG_STYLE_SYSTEM)

        self.panel5 = wx.Panel(id=wxID_PRINTERFRAMEPANEL5, name='panel5',
              parent=self.panel2, pos=wx.Point(32, 0), size=wx.Size(199, 399),
              style=wx.TAB_TRAVERSAL)
        self.panel5.SetBackgroundColour(wx.Colour(255, 255, 255))

        self.panel6 = wx.Panel(id=wxID_PRINTERFRAMEPANEL6, name='panel6',
              parent=self.panel2, pos=wx.Point(231, 0), size=wx.Size(32, 399),
              style=wx.TRANSPARENT_WINDOW | wx.TAB_TRAVERSAL)
        self.panel6.SetBackgroundColour(wx.Colour(240, 240, 240))
        self.panel6.SetBackgroundStyle(wx.BG_STYLE_SYSTEM)
        self.panel6.SetToolTipString('panel6')

        self.Paper = wx.richtext.RichTextCtrl(id=wxID_PRINTERFRAMEPAPER,
              parent=self.panel5, pos=wx.Point(0, 0), size=wx.Size(199, 399),
              style=wx.ALWAYS_SHOW_SB | wx.richtext.RE_READONLY | wx.VSCROLL | wx.richtext.RE_MULTILINE,
              value='\n')
        self.Paper.SetLabel('text')
        self.Paper.SetName('Paper')
        self.Paper.SetToolTipString('Paper')
        self.Paper.SetEditable(False)
        self.Paper.Center(wx.HORIZONTAL)

        self.panel9 = wx.Panel(id=wxID_PRINTERFRAMEPANEL9, name='panel9',
              parent=self.panel3, pos=wx.Point(0, 0), size=wx.Size(32, 299),
              style=wx.TRANSPARENT_WINDOW | wx.TAB_TRAVERSAL)

        self.panel7 = wx.Panel(id=wxID_PRINTERFRAMEPANEL7, name='panel7',
              parent=self.panel3, pos=wx.Point(0, 299), size=wx.Size(32, 100),
              style=wx.TAB_TRAVERSAL)
        self.panel7.SetBackgroundColour(wx.Colour(55, 32, 0))

        self.panel10 = wx.Panel(id=wxID_PRINTERFRAMEPANEL10, name='panel10',
              parent=self.panel6, pos=wx.Point(0, 0), size=wx.Size(32, 299),
              style=wx.TAB_TRAVERSAL)

        self.panel8 = wx.Panel(id=wxID_PRINTERFRAMEPANEL8, name='panel8',
              parent=self.panel6, pos=wx.Point(0, 299), size=wx.Size(32, 100),
              style=wx.TAB_TRAVERSAL)
        self.panel8.SetBackgroundColour(wx.Colour(55, 32, 0))

        self.PowerBtn = wx.BitmapButton(bitmap=wx.Bitmap(u'./PwrBtnOff.png',
              wx.BITMAP_TYPE_PNG), id=wxID_PRINTERFRAMEPOWERBTN,
              name='PowerBtn', parent=self.panel1, pos=wx.Point(32, 32),
              size=wx.Size(72, 40), style=wx.NO_BORDER)
        self.PowerBtn.SetBackgroundColour(wx.Colour(55, 32, 0))
        self.PowerBtn.Bind(wx.EVT_BUTTON, self.OnPowerBtnButton,
              id=wxID_PRINTERFRAMEPOWERBTN)

        self.CrBtn = wx.BitmapButton(bitmap=wx.Bitmap(u'./CrBtn.png',
              wx.BITMAP_TYPE_PNG), id=wxID_PRINTERFRAMECRBTN, name='CrBtn',
              parent=self.panel1, pos=wx.Point(176, 32), size=wx.Size(56, 40),
              style=wx.NO_BORDER)
        self.CrBtn.SetBackgroundColour(wx.Colour(55, 32, 0))
        self.CrBtn.Bind(wx.EVT_BUTTON, self.OnCrBtnButton,
              id=wxID_PRINTERFRAMECRBTN)

        self.StatusBar = wx.StatusBar(id=wxID_PRINTERFRAMESTATUSBAR,
              name='StatusBar', parent=self, style=0)
        self.SetStatusBar(self.StatusBar)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        # Override the RichTextCtrl's Cut, Copy, and Paste methods so that we can support formatted text copy/paste
        self.Paper.Bind(wx.EVT_MENU, self.OnCutCopy, id=wx.ID_CUT)
        self.Paper.Bind(wx.EVT_MENU, self.OnCutCopy, id=wx.ID_COPY)
        self.Paper.Bind(wx.EVT_MENU, self.OnPaste, id=wx.ID_PASTE)        
        
        self.Power=0
        self.Status=0b00000000
        
        self.BitmapPwrOn=wx.Bitmap(u'./PwrBtnOn.png', wx.BITMAP_TYPE_PNG)
        self.BitmapPwrOff=wx.Bitmap(u'./PwrBtnOff.png', wx.BITMAP_TYPE_PNG)




        #Load config file
        #determiniamo la install directory
        (self.basepath,self.script)=os.path.split(sys.argv[0])
        
        #se win i dati vanno nella LOCALAPDATA se altro li mettiamo (per ora) nella installdir 
        if sys.platform.startswith('win'):
            if 'APPDATA' in os.environ:
                self.configpath=os.environ['APPDATA']
            else:
                self.configpath=tempfile.gettempdir()
        else:
            self.configpath=self.basepath
            
        #aggiungiamo TracEv alla configdir in modo da sistemare tutti i dati in una sola cartella    
        self.configpath=os.path.join(self.configpath,'hp82240b')

        #controlliamo se esiste la configdir
        #print self.configpath
        if not os.path.lexists(self.configpath):
            os.mkdir(self.configpath)

        self.configFileName=os.path.join(self.configpath,"config.ini")

        self.config = ConfigParser.RawConfigParser()
        if not os.path.lexists(self.configFileName):
            self.config.add_section('Style')
            self.config.set('Style', 'FontFace', "HP48")
            self.config.set('Style', 'FontSize', '8')

            # Writing our configuration file
            with open(self.configFileName, 'wb') as configfile:
                self.config.write(configfile)
        
        self.config.read(self.configFileName)
        ff = self.config.get('Style', 'FontFace')
        fs = self.config.get('Style', 'FontSize')

        self.HP48Font=wx.Font(int(fs),wx.FONTFAMILY_MODERN,  
                                    wx.FONTSTYLE_NORMAL, 
                                    wx.FONTWEIGHT_NORMAL, 
                                    faceName=ff)

        self.HP48style=wx.richtext.TextAttrEx()

        self.HP48style.SetFont(self.HP48Font)
        self.HP48style.SetAlignment(wx.richtext.TEXT_ALIGNMENT_RIGHT)

        self.HP48styleU=wx.richtext.TextAttrEx()
        self.HP48styleU.SetFont(self.HP48Font)
        self.HP48styleU.SetFontUnderlined(True)

        self.HP48styleB=wx.richtext.TextAttrEx()
        self.HP48styleB.SetFont(self.HP48Font)
        self.HP48styleB.SetFontWeight(wx.FONTWEIGHT_BOLD)

        self.HP48styleUB=wx.richtext.TextAttrEx()
        self.HP48styleUB.SetFont(self.HP48Font)
        self.HP48styleUB.SetFontUnderlined(True)
        self.HP48styleUB.SetFontWeight(wx.FONTWEIGHT_BOLD)

        self.Paper.SetBasicStyle(self.HP48style)
        
        self.ctrl=(-116,"")
        self.THR=None
        
        self.StatusDisconnected()
        
        
        self.ser = serial.Serial()
        self.ConnectTo=0 #0=USB, 1=COM
        
    
    def StatusTry(self):
        text=_("Try to connect calc via: ")
        if self.ConnectTo==0:
            text+=_("USB")
        else:
            #text+=self.ser.name
            text+=_("Serial")+" ("+str(self.ser.portstr)+")"
        self.StatusBar.SetStatusText(text)

    def StatusConnected(self):
        text=_("Connected to calc on: ")
        if self.ConnectTo==0:
            text+=_("USB")
        else:
            #text+=self.ser.name
            text+=_("Serial")+" ("+str(self.ser.portstr)+")"
        self.StatusBar.SetStatusText(text)
           
    def StatusDisconnected(self):
        self.StatusBar.SetStatusText(_("Not connected to calc"))

    def StatusWaitingFor(self):
        self.StatusBar.SetStatusText(_("Waiting for incoming data..."))

    def StatusParsing(self):
        self.StatusBar.SetStatusText(_("Parsing received data..."))

    def StatusNotFound(self):
        text=_("Device not found on: ")
        if self.ConnectTo==0:
            text+=_("USB")
        else:
            #text+=self.ser.name
            text+=_("Serial")+" ("+str(self.ser.portstr)+")"
        self.StatusBar.SetStatusText(text)        


    def OnFileViewHTML(self, evt):
        # Get an instance of the html file handler, use it to save the
        # document to a StringIO stream, and then display the
        # resulting html text in a dialog with a HtmlWindow.
        handler = self.richtext.RichTextHTMLHandler()
        handler.SetFlags(rt.RICHTEXT_HANDLER_SAVE_IMAGES_TO_MEMORY)
        handler.SetFontSizeMapping([7,9,11,12,14,22,100])

        import cStringIO
        stream = cStringIO.StringIO()
        if not handler.SaveStream(self.paper.GetBuffer(), stream):
            return

        

    def OnPollTimerTimer(self, event):
        global sres
        if not self.THR.isAlive():
            self.StatusParsing()
            if self.ctrl[0]==-116: #Il threade' morto per timeout
                res=self.ElaboraStringa(sres)
                sres=""
                if len(res)>0:
                    for j in res:
                        s,i=j
                        if isinstance(i, types.UnicodeType):
                            self.Paper.SetInsertionPointEnd()
                            if s & 0b11 == 0b11:
                                self.Paper.BeginStyle(self.HP48styleUB)
                                self.Paper.WriteText(i)
                                self.Paper.EndStyle()
                            elif s & 0b1 == 0b1:
                                self.Paper.BeginStyle(self.HP48styleB)
                                self.Paper.WriteText(i)
                                self.Paper.EndStyle()
                            elif s & 0b10 == 0b10:
                                self.Paper.BeginStyle(self.HP48styleU)
                                self.Paper.WriteText(i)
                                self.Paper.EndStyle()
                            else:
                                self.Paper.WriteText(i)
                                
                            self.Paper.Newline()
                            self.Paper.ShowPosition(self.Paper.GetLastPosition())
                        elif isinstance(i, Image.Image):
#                            self.Paper.AddImage(PilImageToWxImage( i ))
#                            self.Paper.AppendText("\n")
                            self.Paper.SetInsertionPointEnd()
                            self.Paper.WriteImage(PilImageToWxImage( i ))
                            self.Paper.Newline()
                            self.Paper.ShowPosition(self.Paper.GetLastPosition())
                        else:
                            sys.stderr.write(_("Unknown:"), type(i)) 
                
                #Riavviamo il thread
                self.StatusConnected()
                self.THR=subprocessOutputRead(self.dev)
                self.THR.start()                  
            else: 
                sys.stderr.write(_(u"USB ERROR:")+unicode(str(self.ctrl[0])+" "+self.ctrl[1],'iso-8859-1')+u"\n")
                pass
        else: #Il threas e' vivo 
            pass

                   

        event.Skip()

    def ElaboraStringa(self, sres):
        if len(sres)<0:
            return []
    
        i=0
        bands=[]
        im=Image.new("L",(0,0))
        text=""
        res=[]
        
        while i < len(sres):
         if ord(sres[i])==27: #escape sequence
            i+=1
            if ord(sres[i])<=166: #graphic line:
               #entrabndo len modo grfico verifichiamo se c'e' del testo e mettiamolo
               #nella lista
               if len(text)>0:
                res.append(text)
                text=""
               cols=ord(sres[i])
               i+=1
               if (self.Status & 0b1) == 0b1: #double wide printing
                   s=im.size
                   #print "S1",s
                   if s[0]<cols*2:
                      im=im.crop((0,0,cols*2,s[1]+8))
                   else:
                      im=im.crop((0,0,s[0],s[1]+8))
                   s=im.size
                   #putpixel = im.im.putpixel
                   #print "S2",s
                   for b in range(8):
                      for c in range(cols):
                         try:
                            if 0x2**b==(0x2**b & ord(sres[i+c])):
                               im.putpixel((c*2,s[1]-8+b),0)
                               im.putpixel((c*2+1,s[1]-8+b),0)
                               #sys.stdout.write("#")
                            else:
                               im.putpixel((c*2,s[1]-8+b),255)
                               im.putpixel((c*2+1,s[1]-8+b),255)
                               #sys.stdout.write(" ")
                         except IndexError:
                            sys.stderr.write(_("Out of bound:"), im.size,(c,s[1]-8+b))
                            
               else:
                   s=im.size
                   #print "S1",s
                   if s[0]<cols:
                      im=im.crop((0,0,cols,s[1]+8))
                   else:
                      im=im.crop((0,0,s[0],s[1]+8))
                   s=im.size
                   #putpixel = im.im.putpixel
                   #print "S2",s
                   for b in range(8):
                      for c in range(cols):
                         try:
                            if 0x2**b==(0x2**b & ord(sres[i+c])):
                               im.putpixel((c,s[1]-8+b),0)
                               #sys.stdout.write("#")
                            else:
                               im.putpixel((c,s[1]-8+b),255)
                               #sys.stdout.write(" ")
                         except IndexError:
                            sys.stderr.write(_("Out of bound:"), im.size,(c,s[1]-8+b))
                            

               i+=cols
               pass
            elif ord(sres[i])==255: #printer reset
               i+=1
               self.Status=0b0
               #print "#printer reset"
               pass
            elif ord(sres[i])==254: #printer self test
               i+=1
               #print "printer self test"
               pass
            elif ord(sres[i])==253: #double wide mode
               self.Status=self.Status | 0b1 
               i+=1
               #print "double wide mode"
               pass
            elif ord(sres[i])==252: #single wide mode
               self.Status=self.Status & 0b11111110 
               i+=1
               #print "single wide mode"
               pass
            elif ord(sres[i])==251: #start underlining
               self.Status=self.Status | 0b10 
               i+=1
               #print "start underlining"
               pass
            elif ord(sres[i])==250: #stop underlining:
               self.Status=self.Status & 0b11111101 
               i+=1
               #print "stop underlining"
               pass
            elif ord(sres[i])==249: #Start printing in the ISO 8859 Latin 1
               self.Status=self.Status & 0b11111011 
               i+=1
               #print "Start printing in the ISO 8859 Latin 1"
               pass
            elif ord(sres[i])==248: #Start printing in the Roman8 character set
               self.Status=self.Status | 0b100 
               i+=1
               #print "Start printing in the Roman8 character set"
               pass
            else:
               i+=1
               #print "Sconosciuto!"
               pass
         elif ord(sres[i])==128 and ord(sres[i+1])==119:
            i+=1
            sys.stderr.write("BOH! %i\n" % ord(sres[i]))
            i+=1
            sys.stderr.write("BOH! %i\n" % ord(sres[i]))
            i+=1
            sys.stderr.write("BOH! %i\n" % ord(sres[i]))
            i+=1
            rows=ord(sres[i])
            i+=1
            cols=ord(sres[i])
            i+=1

            im=Image.new("L",(cols,rows))
            r=int(math.ceil(cols/8.0))

            for w in range(rows):
                for j in range(r):

                    for k in range(4):
                        if j*8+k<cols:
                            c=ord(sres[i])
                            if 0x2**k==(0x2**k & c):
                                #sys.stdout.write(format(0x2**k & c,"X"))
                                im.putpixel((j*8+k,w),0)
                            else:
                                im.putpixel((j*8+k,w),255)

                    for k in range(4):
                        if j*8+k+4<cols:
                            c=ord(sres[i]) >> 4
                            if 0x2**k==(0x2**k & c):
                                #sys.stdout.write(format(0x2**k & c,"X"))
                                im.putpixel((j*8+k+4,w),0)
                            else:
                                im.putpixel((j*8+k+4,w),255)
                    i+=1    

         elif ord(sres[i])==4 or ord(sres[i])==10 or ord(sres[i])==13:
            #verifichiamo se c'e' del testo e mettiamolo
            #nella lista
            if len(text)>0:
                res.append((self.Status,text))
                text=""     
                #sys.stdout.write("\n")#[0:len(sres)-2]
            i+=1
         else:         
            if im.size != (0,0):
                res.append((self.Status,im))
                im=Image.new("L",(0,0))
            text+=sres[i]
            
            #sys.stdout.write(sres[i])#[0:len(sres)-2]
            i+=1
            pass

        if len(text)>0:
            res.append((self.Status,text))
            text=""     
            #sys.stdout.write("\n")#[0:len(sres)-2]
        if im.size != (0,0):
            res.append((self.Status,im))
            im=Image.new("L",(0,0))
                
        sres=""
        del im
        
        return res

    def OnStaticBitmap1MouseEvents(self, event):
        self.staticBitmap1.SetBitmap(wx.Bitmap(u'C:/Users/Public/Progz/TracEv/icons/meno.png'))
        event.Skip()

    def GetFormattedSelection(self, format):
        """ Return a string with the formatted contents of just the RichText control's current selection.
            format can either be 'XML' or 'RTF'. """
        # If a valid format is NOT passed ...
        if not format in ['XML', 'RTF']:
            # ... return a blank string
            return ''
        
        # NOTE:  The wx.RichTextCtrl doesn't provide an easy way to get a formatted selection that I can figure out.
        #        This is a hack, but it works.

        # Freeze the control so things will work faster
        self.Paper.Freeze()
        # We'll need to undo everything from this point on.  Let's do it as ONE operation.
        self.Paper.BeginBatchUndo('RTCBufferSelection')
        # Get the start and end of the current selection
        sel = self.Paper.GetSelection()
        # Delete everything AFTER the end of the current selection
        self.Paper.Delete((sel[1], self.Paper.GetLastPosition()))
        # Delete everything BEFORE the start of the current selection
        self.Paper.Delete((0, sel[0]))
        # This leaves us with JUST the selection in the control!

        # If XML format is requested ...
        if format == 'XML':
            # Create a Stream
            stream = cStringIO.StringIO()
            # Get an XML Handler
            handler = richtext.RichTextXMLHandler()
            # Save the contents of the control to the stream
            handler.SaveStream(self.Paper.GetBuffer(), stream)
            # Convert the stream to a usable string
            tmpBuffer = stream.getvalue()
        # If RTF format is requested ....
        elif format == 'RTF':
            # Get an RTF Handler
            handler = PyRTFParser.PyRichTextRTFHandler()
            # Get the string representation by leaving off the filename parameter
            tmpBuffer = handler.SaveFile(self.Paper.GetBuffer())

        # End Undo batching
        self.Paper.EndBatchUndo()
        # Undo the changes.  This restores ALL the text
        self.Paper.Undo()
        # Now thaw the control so that updates will be displayed again
        self.Paper.Thaw()
            
        # Return the buffer's XML string
        return tmpBuffer

              
    def OnCutCopy(self, event):
        """ Handle Cut and Copy events, over-riding the RichTextCtrl versions.
            This implementation supports Rich Text Formatted text, and at least on Windows can
            share formatted text with other programs. """
        # Create a Composite Data Object for the Clipboard
        compositeDataObject = wx.DataObjectComposite()

        # Get the current selection in RTF format
        rtfSelection = self.GetFormattedSelection('RTF')
        # Create a Custom Data Format for RTF
        if 'wxMac' in wx.PlatformInfo:
            rtfFormat = wx.CustomDataFormat('public.rtf')
        else:
            # Create a Custom Data Format for RTF
            rtfFormat = wx.CustomDataFormat('Rich Text Format')
        # Create a Custom Data Object for the RTF format
        rtfDataObject = wx.CustomDataObject(rtfFormat)
        # Save the RTF version of the control selection to the RTF Custom Data Object
        rtfDataObject.SetData(rtfSelection)
        # Add the RTF Custom Data Object to the Composite Data Object
        compositeDataObject.Add(rtfDataObject)

        # Get the current selection in Plain Text
        txtSelection = self.Paper.GetStringSelection()
        # Create a Text Data Object
        txtDataObject = wx.TextDataObject()
        # Save the Plain Text version of the control selection to the Text Data Object
        txtDataObject.SetText(txtSelection)
        # Add the Plain Text Data Object to the Composite Data object
        compositeDataObject.Add(txtDataObject)

        # Open the Clipboard
        if wx.TheClipboard.Open():
            # Place the Composite Data Object (with RTF and Plain Text) on the Clipboard
            wx.TheClipboard.SetData(compositeDataObject)
            # Close the Clipboard
            wx.TheClipboard.Close()

        # If we are CUTting (rather than COPYing) ...
        if event.GetId() == wx.ID_CUT:
            # ... delete the selection from the Rich Text Ctrl.
            self.Paper.DeleteSelection()

    def OnPaste(self, event):
        """ Handle Paste events, over-riding the RichTextCtrl version.
            This implementation supports Rich Text Formatted text, and at least on Windows can
            share formatted text with other programs. """
        # Open the Clipboard
        if wx.TheClipboard.Open():
            if 'wxMac' in wx.PlatformInfo:
                rtfFormat = wx.CustomDataFormat('public.rtf')
            else:
                # Create a Custom Data Format for RTF
                rtfFormat = wx.CustomDataFormat('Rich Text Format')
            # See if the RTF Format is supported by the current clipboard data object
            if wx.TheClipboard.IsSupported(rtfFormat):
                # Specify that the data object accepts data in RTF format
                customDataObject = wx.CustomDataObject(rtfFormat)
                # Try to get data from the Clipboard
                success = wx.TheClipboard.GetData(customDataObject)
                # If the data in the clipboard is in an appropriate format ...
                if success:
                    # ... get the data from the clipboard
                    formattedText = customDataObject.GetData()
                    # Prepare the control for data
                    self.Paper.Freeze()
                    self.Paper.BeginSuppressUndo()
                    # Start exception handling
                    try:
                        # Use the custom RTF Handler
                        handler = PyRTFParser.PyRichTextRTFHandler()
                        # Load the RTF data into the Rich Text Ctrl via the RTF Handler.
                        # Note that for RTF, the wxRichTextCtrl CONTROL is passed with the RTF string.
                        handler.LoadString(self.Paper, formattedText)
                    # exception handling
                    except:
                        print "Custom RTF Handler Load failed"
                        print
                        print sys.exc_info()[0], sys.exc_info()[1]
                        print traceback.print_exc()
                        print
                        pass

                    # Signal the end of changing the control
                    self.Paper.EndSuppressUndo()
                    self.Paper.Thaw()
            # If there's not RTF data, see if there's Plain Text data
            elif wx.TheClipboard.IsSupported(wx.DataFormat(wx.DF_TEXT)):
                # Create a Text Data Object
                textDataObject = wx.TextDataObject()
                # Get the Data from the Clipboard
                wx.TheClipboard.GetData(textDataObject)
                # Write the plain text into the Rich Text Ctrl
                self.Paper.WriteText(textDataObject.GetText())
            # Close the Clipboard
            wx.TheClipboard.Close()

    def OnPowerBtnButton(self, event):
        if self.Power==0:
            
            self.StatusTry()
            
            if self.ConnectTo==0:
                # find our device
                self.dev = usb.core.find(idVendor=0x03F0, idProduct=0x0121)
                #dev = usb.core.find(idVendor=0x067B, idProduct=0x2303)
                #dev = usb.core.find(idVendor=0x0506, idProduct=0x00DF)

                #dev = usb.core.find(idVendor=0x0, idProduct=0x0)

                # was it found?
                if self.dev is None:
                    self.StatusNotFound()
                    return
                
                    raise ValueError(_('Device not found'))

                self.StatusConnected()
##
##                print self.dev.__dict__
##
##
##                    
##                for k,v in self.dev.__dict__.items():
##                    print k,
##                    try:
##                        print '0x'+format(v,'x')
##                    except:
##                        print v
##
##                print "### cfg ###"  
##                for i in self.dev:
##                    for k,v in i.__dict__.items():
##                        print k,
##                        try:
##                            print '0x'+format(v,'x')
##                        except:
##                            print v
##                    for j in i:
##                        print "    #### intf ####"        
##                        for k,v in j.__dict__.items():
##                            print "    ",k,
##                            try:
##                                print '0x'+format(v,'x')
##                            except:
##                                print "    ",v
##                        for w in j:
##                            print "         #### ep ####", "0x"+format(usb.util.endpoint_direction(w.bEndpointAddress), 'x'),
##                            if usb.util.endpoint_direction(w.bEndpointAddress)==usb.util.ENDPOINT_OUT:
##                                print " OUT"
##                            elif usb.util.endpoint_direction(w.bEndpointAddress)==usb.util.ENDPOINT_IN:
##                                print " IN"
##                            else:
##                                print " BOH!"
##
##                            for k,v in w.__dict__.items():
##                                print "        ",k,
##                                try:
##                                    print '0x'+format(v,'x')
##                                except:
##                                    print "    ",v
##
##                  
##                for cfg in self.dev:
##                    sys.stdout.write(format(cfg.bConfigurationValue,'x') + '\n')
##                    for intf in cfg:
##                        sys.stdout.write('\t' + \
##                                         format(intf.bInterfaceNumber,'x') + \
##                                         ',' + \
##                                         format(intf.bAlternateSetting,'x') + \
##                                         '\n')
##                        for ep in intf:
##                            sys.stdout.write('\t\t' + \
##                                             format(ep.bEndpointAddress, 'x') + \
##                                             '\n')        


                self.dev.set_configuration()
            else:
                self.ser.timeout=0.3
                self.dev=self.ser
                self.dev.open()
                
            self.Power=1
            self.menuBar1.EnableTop(2, False)
            self.PowerBtn.SetBitmapLabel(self.BitmapPwrOn)
            self.THR=subprocessOutputRead(self.dev)
            self.THR.start()              
            self.PollTimer.Start(100)            
           
            #self.PowerBtn.SetBitmapHover(self.BitmapPwrOn)
            #self.PowerBtn.SetBitmapSelected(self.BitmapPwrOn)
            #self.PowerBtn.SetBitmapFocus(self.BitmapPwrOn)
        else:
            self.Power=0
            self.PowerBtn.SetBitmapLabel(self.BitmapPwrOff)
            self.PollTimer.Stop() 
            del self.dev
            self.StatusDisconnected()           
            self.menuBar1.EnableTop(2, True)
            
            
        event.Skip()

    def OnCrBtnButton(self, event):
        self.Paper.Newline()
        self.Paper.ShowPosition(self.Paper.GetLastPosition())
        event.Skip()


    def OnConnectMnuItems0Menu(self, event): #USB
        self.ConnectTo=0
        event.Skip()

    def OnConnectMnuItems1Menu(self, event): #Serial
        self.ConnectTo=1
        dialog_serial_cfg = SerialConfigDialog(None, -1, "", serial=self.ser)
        #self.SetTopWindow(dialog_serial_cfg)
        result = dialog_serial_cfg.ShowModal()  
        #event.Skip()

    def OnHelpMnuInfoMenu(self, event):
        info = wx.AboutDialogInfo()
        info.Name = "HP82240B Printer emulator"
        info.Version = _SW_RELEASE
        info.Copyright = "(C) 2010 Alfredo Branco"
        info.Description = wordwrap(_(u"HP82240B Printer emulator\n")+ 
                                    _(u"Hp thermal infrared printer emulator ")+
                                    _(u"Prints on RTF page with Copy/Paste ability ")+
                                    _(u"connect on USB/COM Ports."),350, wx.ClientDC(self))
        info.WebSite = ("http://www.ingbranco.com/alfredo/HP82440Bemu.html", _("Web site"))
        info.Developers = [ "Alfredo Branco", ]
        licenseText=''
        info.License = wordwrap(licenseText, 500, wx.ClientDC(self))

        # Then we call wx.AboutBox giving it that info object
        wx.AboutBox(info)

    def OnEditMnuMenu_edit_clearMenu(self, event):
        dlg=wx.MessageDialog(self, _("Trashing the paper. Are you sure?"), 
                        "WARNING", wx.OK | wx.CANCEL | wx.ICON_EXCLAMATION)
        if dlg.ShowModal() == wx.ID_OK:
            self.Paper.Clear()

    def OnFileMnuFontitemMenu(self, event):
        data=wx.FontData()
        data.SetInitialFont(self.HP48Font)
        dlg=wx.FontDialog(self, data)
        if dlg.ShowModal() == wx.ID_OK:
            data=dlg.GetFontData()
            self.HP48Font=data.GetChosenFont()
            
            self.HP48style.SetFont(self.HP48Font)
            self.HP48style.SetAlignment(wx.richtext.TEXT_ALIGNMENT_RIGHT)

            self.HP48styleU=wx.richtext.TextAttrEx()
            self.HP48styleU.SetFont(self.HP48Font)
            self.HP48styleU.SetFontUnderlined(True)

            self.HP48styleB=wx.richtext.TextAttrEx()
            self.HP48styleB.SetFont(self.HP48Font)
            self.HP48styleB.SetFontWeight(wx.FONTWEIGHT_BOLD)

            self.HP48styleUB=wx.richtext.TextAttrEx()
            self.HP48styleUB.SetFont(self.HP48Font)
            self.HP48styleUB.SetFontUnderlined(True)
            self.HP48styleUB.SetFontWeight(wx.FONTWEIGHT_BOLD)

            self.Paper.SetBasicStyle(self.HP48style)            

            self.config.set('Style', 'FontFace', self.HP48Font.GetFaceName())
            self.config.set('Style', 'FontSize', str(self.HP48Font.GetPointSize()))

            # Writing our configuration file to 'example.cfg'
            with open(self.configFileName, 'wb') as configfile:
                self.config.write(configfile)
            